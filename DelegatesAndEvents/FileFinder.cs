﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents
{
    public class FileFoundArgs : EventArgs
    {
        public string FoundFile { get; }
        public bool CancelRequested { get; set; }

        public FileFoundArgs(string fileName) => FoundFile = fileName;
    }

    public class FileFinder
    {
        public event EventHandler<FileFoundArgs> FileFound;

        public void Find(string directory)
        {
            foreach (var file in Directory.EnumerateFiles(directory))
            {
                FileFoundArgs args = RaiseFileFound(file);
                if (args.CancelRequested)
                {
                    break;
                }
            }
        }

        private FileFoundArgs RaiseFileFound(string file)
        {
            var args = new FileFoundArgs(file);
            FileFound?.Invoke(this, args);
            return args;
        }
    }
}
