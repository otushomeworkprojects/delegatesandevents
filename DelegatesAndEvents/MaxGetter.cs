﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents
{
    public static class MaxGetter
    {
        public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            var maxFloat = float.MinValue;
            T max = null;

            foreach (T item in e)
            {
                var itemFloat = getParameter(item);
                if (itemFloat > maxFloat)
                {
                    maxFloat = itemFloat;
                    max = item;
                }
            }

            return max;
        }
    }
}
